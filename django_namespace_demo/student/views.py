from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import redirect,reverse

def index(request):
    current_namespace = request.resolver_match.namespace   
    print(current_namespace)
    if request.GET.get("username"):
        return HttpResponse("Student List Page")
    else:
        return redirect(reverse(f"{current_namespace}:login"))

def login(request):
    url_path = request.path
    return HttpResponse(f"Login Page and You need to Login first! (当前请求路径为: {url_path})")
