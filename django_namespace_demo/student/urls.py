from django.urls import path
from student import views

# app_name = 'student'
urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
]