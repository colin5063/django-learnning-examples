from tabnanny import verbose
from django.db import models


# Create your models here.
class TestMedia(models.Model):
    name = models.CharField(max_length=32, verbose_name="专题名称")
    cover = models.ImageField(upload_to='covers/', verbose_name="专题封面")
    video = models.FileField(upload_to='vedios/', verbose_name="专题视频")

    def __str__(self):
        return self.name 
        