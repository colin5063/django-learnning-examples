from rest_framework_bulk.routes import BulkRouter
from rest_framework.routers import SimpleRouter

from demoapp.views import UserViewSet
from demoapp.views import StudentViewSet
from demoapp.selfaction import UserModelViewSet

from django.urls import path, include



router = BulkRouter()
router.register('users', UserViewSet)
router.register('students', StudentViewSet)

router2 = SimpleRouter()
router2.register('cusers', UserModelViewSet)

urlpatterns = [
    path('v1/', include(router2.urls)),
    path('v2/', include(router.urls)),
]
