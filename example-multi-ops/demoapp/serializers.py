# serializers.py
from rest_framework.serializers import ModelSerializer
from rest_framework.filters import SearchFilter
from rest_framework_bulk import BulkListSerializer, BulkSerializerMixin, BulkModelViewSet

from demoapp.models import User
from demoapp.models import Student


class UserSerializer(BulkSerializerMixin, ModelSerializer):
    # print("in UserSerializer")
    class Meta:
        model = User
        fields = '__all__'
        list_serializer_class = BulkListSerializer
        filter_backends = (SearchFilter, )
        

class StudentSerializer(BulkSerializerMixin, ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'
        list_serializer_class = BulkListSerializer
        filter_backends = (SearchFilter, )
