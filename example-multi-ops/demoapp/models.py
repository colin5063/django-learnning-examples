from django.db import models


class User(models.Model):
    name = models.CharField(max_length=32, unique=True, verbose_name='用户名')
    age = models.IntegerField(default=18)

    def __str__(self):
        return self.name 


# Student 是后来添加的，`User` 在使用restframework-bulk 模块的时候存在问题
# 注意这里两个模型存在的差异性，你发现差在哪里了吗？
class Student(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name
