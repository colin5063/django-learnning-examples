# fitlers.py
from django_filters.rest_framework.filterset import FilterSet
from django_filters import filters

from demoapp.models import User
from demoapp.models import Student


class UserFilterSet(FilterSet):
    class Meta:
        model = User
        fields = {
            'id': ['in'],
            'name': ['icontains'],
        }


class StudentFilterSet(FilterSet):
    class Meta:
        model = Student
        fields = {
            'id': ['in'],
            'name': ['icontains'],
        }
