# views.py
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework_bulk import BulkModelViewSet

from demoapp.models import User, Student
from demoapp.serializers import UserSerializer, StudentSerializer
from demoapp.filters import UserFilterSet, StudentFilterSet


# Create your views here.
class UserViewSet(BulkModelViewSet):
    # print("in UserViewSet")
    serializer_class = UserSerializer
    queryset = User.objects.all()
    # print(queryset)
    filter_backends = (DjangoFilterBackend, )
    filterset_class = UserFilterSet

    def allow_bulk_destroy(self, qs, filtered):
        # self.filter_queryset(qs)
        # qs = self.get_queryset()
        # filtered = self.filter_queryset(qs)
        print("here: qs-> ", qs, "\n filtered-> ", filtered)
        return qs is not filtered
        

class StudentViewSet(BulkModelViewSet):
    serializer_class = StudentSerializer
    queryset = Student.objects.all()
    # print(queryset)
    filter_backends = (DjangoFilterBackend, )
    filterset_class = StudentFilterSet

    def allow_bulk_destroy(self, qs, filtered):
        print("here: qs-> ", qs, "\n filtered-> ", filtered)
        return qs is not filtered

