

1、下载代码

```bash
git clone https://gitee.com/colin5063/django-learnning-examples.git

cd django-learnning-examples
git checkout example-multi-ops
```

2、创建表执行初始化

```bash
cd example-multi-ops
# 表结构同步
python manager.py makemigrations
python manager.py migrate 

# 创建管理员账号，然后再后台手动添加初始数据
python manager.py createsuperuser

# 或者跳过上面创建管理员账号，直接启动服务之后，通过接口创建
python manager.py runserver 
```

3、接口说明

|方法|api接口|参数|说明|
|---|---|---|---|
|GET	|http://127.0.0.1:8001/api/students/	|	|获取列表
|GET	|http://127.0.0.1:8001/api/students/1/	|	|获取单个记录
|GET	|http://127.0.0.1:8001/api/students/	|?id__in=2,3&name__icontains=	自定义过滤查询
|POST	|http://127.0.0.1:8001/api/students/	|{ ‘name’: ‘Stu-01’}	新增单条记录
|POST	|http://127.0.0.1:8001/api/students/	|[{ ‘name’: ‘Stu-02’},{ ‘name’: ‘Stu-03’}]	批量新增
|PUT/PATCH	|http://127.0.0.1:8001/api/students/	|{ ‘id’: 1, ‘name’: ‘Stu-01-ch’}	单个更新
|PUT/PATCH	|http://127.0.0.1:8001/api/students/	|[{ ‘id’: 2, ‘name’: ‘Stu-02-ch’},{ ‘id’: 3, ‘name’: ‘Stu-03-ch’}]	|批量更新
|DELETE	|http://127.0.0.1:8001/api/students/1/	|	|单个删除
|DELETE	|http://127.0.0.1:8001/api/students/	|?id__in=2,3	|批量删除


博客地址：  http://blog.colinspace.com/2022/04/28/20220428-Django-restframework%E5%AE%9E%E7%8E%B0%E6%89%B9%E9%87%8F%E6%93%8D%E4%BD%9C/


---


博客首页:    [http://blog.colinspace.com](http://blog.colinspace.com)
csdn首页:   [https://blog.csdn.net/eagle5063](https://blog.csdn.net/eagle5063)
简书首页:    [https://www.jianshu.com/u/6d793fbacc88](https://www.jianshu.com/u/6d793fbacc88)
知乎首页:   [https://www.zhihu.com/people/colin-31-49](https://www.zhihu.com/people/colin-31-49)
