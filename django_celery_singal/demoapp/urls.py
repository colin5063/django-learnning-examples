from django.urls import path
from demoapp import views 

urlpatterns = [
    path('demo/', views.demo, name='demo-task'),
]