import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_celery_singal.settings')
app = Celery(__name__)

# 从Django的settings.py加载 celery的配置
app.config_from_object('django.conf:settings', namespace='CELERY')
# 自动发现应用中的tasks（应用中的tasks.py文件中定义的任务）
app.autodiscover_tasks()