from celery import shared_task

@shared_task
def task_hello():
    print('Hello Task under demoapp')
    return 'ok'