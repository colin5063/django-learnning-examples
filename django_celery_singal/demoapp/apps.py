from django.apps import AppConfig


class DemoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'demoapp'

    def ready(self):
        from demoapp.celery import singal_handler
        super().ready()