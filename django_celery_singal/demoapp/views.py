from django.http import HttpResponse
from demoapp.tasks import task_hello

def demo(request):
    task_hello.delay()
    return HttpResponse("Task Executed")