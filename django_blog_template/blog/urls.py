# encoding: utf-8

from django.urls import path
from blog import views

app_name = 'blog'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:id>/', views.detail, name='detail'),
    path('tags/', views.tag, name='tags'),
    path('categories/', views.category, name='categories'),
    path('tag/<int:id>/', views.tag_posts, name='tag_posts'),
    path('category/<int:id>/', views.category_posts, name='category_posts'),
    path('archive/', views.archive, name='archive'),
]