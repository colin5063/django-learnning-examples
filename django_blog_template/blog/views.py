from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'blog/index.html')


def detail(request, id):
    return render(request, 'blog/detail.html')


def category(request):
    return render(request, 'blog/category.html')


def tag(request):
    return render(request, 'blog/tag.html')


def category_posts(request, id):
    return render(request, 'blog/category_posts.html')


def tag_posts(request, id):
    return render(request, 'blog/tag_posts.html')


def archive(request):
    return render(request, 'blog/archive.html')